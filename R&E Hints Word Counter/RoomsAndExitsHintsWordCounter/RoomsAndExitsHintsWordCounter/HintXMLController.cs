﻿using System.Xml.Linq;

namespace RoomsAndExitsHintsWordCounter
{
    internal class HintXMLController
    {
        public OnHintWordsCounted? onHintWordsCounted;
        public OnHintWordsCounted? onHintHelperWordsCounted;
        public OnHintWordsCounted? onVagueWordsCounted;

        private int hintWordCount = 0;
        private int vagueWordCount = 0;
        private int helperWordCount = 0;

        public void LoadXMLFileAndTexts(string filePath, RichTextBox logTextBox)
        {
            XDocument? xml = XDocument.Load(filePath);
            logTextBox.Text += $"Loaded file: {filePath}\n\n";

            IEnumerable<string> query = from singleHint in xml?.Root?.Descendants("hint")
                        from levelText in singleHint.Elements("level")
                        where (string?)levelText?.Attribute("type") == "text"
                        select levelText.Value;

            CountWords(query.ToList(), logTextBox, "Hint");

            IEnumerable<string> queryVague = from singleHint in xml?.Root?.Descendants("hint")
                                             from levelText in singleHint.Elements("level")
                                             where (string?)levelText?.Attribute("type") == "vagueText"
                                             select levelText.Value;

            CountWords(queryVague.ToList(), logTextBox, "Vague");

            IEnumerable<string> queryHelpers = from allHelpers in xml?.Root?.Descendants("helpers")
                               from helpers in allHelpers.Elements("helper")
                               select helpers.Value;

            CountWords(queryHelpers.ToList(), logTextBox, "Helper");

            logTextBox.Text += $"Hints + Vague: {hintWordCount + vagueWordCount}\nHints + Vague + Helpers: {hintWordCount + vagueWordCount + helperWordCount}\n-----------------------------------------------------------------------------\n\n";
        }

        public void CountWords(List<string> query, RichTextBox logTextBox, string textType)
        {
            logTextBox.Text += $"{textType} count: {query.Count}";
            if(textType == "Vague")
            {
                logTextBox.Text += " (Should be the same number as Hint count!)";
            }

            List<string> allWords = new List<string>();
            foreach (string text in query)
            {
                string[] words = text.Split(' ');
                allWords.AddRange(words);
            }

            switch(textType)
            {
                case "Hint":
                    hintWordCount = allWords.Count;
                    break;
                case "Vague":
                    vagueWordCount = allWords.Count;
                    break;
                case "Helper":
                    helperWordCount = allWords.Count;
                    break;
            }
            
            logTextBox.Text += $"\n{textType} word count: {allWords.Count}\n\n";
        }
    }
}
