﻿namespace RoomsAndExitsHintsWordCounter
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            label1 = new Label();
            xmlFileDialog = new OpenFileDialog();
            FileDlgBtn = new Button();
            label2 = new Label();
            fileNameLabel = new Label();
            logTextBox = new RichTextBox();
            folderDialogButton = new Button();
            label3 = new Label();
            folderBrowserDialog = new FolderBrowserDialog();
            label4 = new Label();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(7, 6);
            label1.Name = "label1";
            label1.Size = new Size(134, 15);
            label1.TabIndex = 0;
            label1.Text = "Select Hint XML file (en)";
            // 
            // xmlFileDialog
            // 
            xmlFileDialog.FileName = "Hints_en";
            // 
            // FileDlgBtn
            // 
            FileDlgBtn.Location = new Point(7, 24);
            FileDlgBtn.Name = "FileDlgBtn";
            FileDlgBtn.Size = new Size(134, 32);
            FileDlgBtn.TabIndex = 1;
            FileDlgBtn.Text = "File";
            FileDlgBtn.UseVisualStyleBackColor = true;
            FileDlgBtn.Click += FileDlgBtn_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(183, 6);
            label2.Name = "label2";
            label2.Size = new Size(76, 15);
            label2.TabIndex = 2;
            label2.Text = "Loaded Path:";
            // 
            // fileNameLabel
            // 
            fileNameLabel.AutoSize = true;
            fileNameLabel.Location = new Point(183, 24);
            fileNameLabel.Name = "fileNameLabel";
            fileNameLabel.Size = new Size(107, 15);
            fileNameLabel.TabIndex = 3;
            fileNameLabel.Text = "Expected: Hints_en";
            // 
            // logTextBox
            // 
            logTextBox.Location = new Point(183, 59);
            logTextBox.Name = "logTextBox";
            logTextBox.ReadOnly = true;
            logTextBox.Size = new Size(498, 424);
            logTextBox.TabIndex = 6;
            logTextBox.Text = "";
            // 
            // folderDialogButton
            // 
            folderDialogButton.Location = new Point(7, 100);
            folderDialogButton.Name = "folderDialogButton";
            folderDialogButton.Size = new Size(134, 32);
            folderDialogButton.TabIndex = 7;
            folderDialogButton.Text = "Folder";
            folderDialogButton.UseVisualStyleBackColor = true;
            folderDialogButton.Click += FolderDialogButtonClick;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(7, 67);
            label3.Name = "label3";
            label3.Size = new Size(117, 15);
            label3.TabIndex = 8;
            label3.Text = "Select Chapter folder";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(7, 82);
            label4.Name = "label4";
            label4.Size = new Size(107, 15);
            label4.TabIndex = 9;
            label4.Text = "Use this - it's easier";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.GradientActiveCaption;
            ClientSize = new Size(693, 495);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(folderDialogButton);
            Controls.Add(logTextBox);
            Controls.Add(fileNameLabel);
            Controls.Add(label2);
            Controls.Add(FileDlgBtn);
            Controls.Add(label1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Form1";
            Text = "Rooms And Exits Hints Word Counter";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private OpenFileDialog xmlFileDialog;
        private Button FileDlgBtn;
        private Label label2;
        private Label fileNameLabel;
        private RichTextBox logTextBox;
        private Button folderDialogButton;
        private Label label3;
        private FolderBrowserDialog folderBrowserDialog;
        private Label label4;
    }
}