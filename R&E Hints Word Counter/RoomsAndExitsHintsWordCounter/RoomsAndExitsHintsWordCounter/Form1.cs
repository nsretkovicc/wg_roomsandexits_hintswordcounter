namespace RoomsAndExitsHintsWordCounter
{
    public delegate void OnFileSelected(string filePath);
    public delegate void OnHintWordsCounted(int wordsNumber, string filePath);
    public partial class Form1 : Form
    {
        FileLoadingController fileLoadingController;
        HintXMLController hintXMLController;

        public Form1()
        {
            InitializeComponent();
            fileLoadingController = new FileLoadingController();
            hintXMLController = new HintXMLController();
        }

        private void FileDlgBtn_Click(object sender, EventArgs e)
        {
            fileLoadingController.OpenFileDialog(xmlFileDialog, OnFileSelectedLoadXML);
        }
        private void FolderDialogButtonClick(object sender, EventArgs e)
        {
            fileLoadingController.OpenFolderDialog(folderBrowserDialog, OnFileSelectedLoadXML, PrintFileNotFound, logTextBox);
        }

        private void OnFileSelectedLoadXML(string filePath)
        {
            //logTextBox.Text += filePath + "\n\n";

            hintXMLController.LoadXMLFileAndTexts(filePath, logTextBox);
        }

        private void PrintFileNotFound(string filePath)
        {
            logTextBox.Text += $"Hints_en.xml not found on path: ${filePath}\n-----------------------------------------------------------------------------\n\n";
        }
    }
}