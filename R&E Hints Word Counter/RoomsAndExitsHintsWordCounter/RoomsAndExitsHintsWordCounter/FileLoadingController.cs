﻿namespace RoomsAndExitsHintsWordCounter
{
    internal class FileLoadingController
    {

        public void OpenFileDialog(FileDialog dialog, Action<string> onHintFileSelected)
        {
            dialog.DefaultExt = "xml";
            dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            dialog.CheckFileExists = true;
            dialog.CheckPathExists = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if(dialog.FileName.Contains("Hints_en"))
                {
                    Console.WriteLine(dialog.InitialDirectory);
                    onHintFileSelected.Invoke(dialog.FileName);
                }
                else
                {
                    MessageBox.Show("You selected file that is not \"Hints_en.xml\", only \"en files can be selected\".", "Wrong Hint File Selected",MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void OpenFolderDialog(FolderBrowserDialog dialog, Action<string> onHintFileSelected, Action<string> onHintFileNotFound, RichTextBox logTextBox)
        {
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                logTextBox.Text = string.Empty;
                string selectedFolder = dialog.SelectedPath;

                // Start searching for "Hints_en.xml" files in the selected folder
                SearchForHintFiles(selectedFolder, onHintFileSelected, onHintFileNotFound);
            }
        }

        public void SearchForHintFiles(string folderPath, Action<string> onHintFileSelected, Action<string> onHintFileNotFound)
        {
            try
            {
                // Search for "Hints_en.xml" files in the current folder
                string hintsFilePath = Path.Combine(folderPath, "Hints_en.xml");

                if (File.Exists(hintsFilePath))
                {
                    // Invoke the delegate when the file is found
                    onHintFileSelected.Invoke(hintsFilePath);
                }
                else
                {
                    // The file does not exist in the current folder
                    onHintFileNotFound.Invoke(folderPath);
                }

                // Recursively search in subfolders
                foreach (string subfolder in Directory.GetDirectories(folderPath))
                {
                    SearchForHintFiles(subfolder, onHintFileSelected, onHintFileNotFound);
                }
            }
            catch (Exception ex)
            {
                // Handle any potential exceptions here, e.g., access denied
                MessageBox.Show(ex.ToString(), "Error occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
